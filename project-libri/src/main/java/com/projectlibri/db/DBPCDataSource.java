package com.projectlibri.db;

import java.sql.Connection;
import java.sql.SQLException;

import org.apache.commons.dbcp2.BasicDataSource;


public class DBPCDataSource {
	
	private static BasicDataSource ds = new BasicDataSource();
	
	static {
		try {
		ds.setUrl(System.getProperty("db.url"));
		ds.setUsername(System.getProperty("db.user"));
		ds.setPassword(System.getProperty("db.password"));
        ds.setMinIdle(Integer.parseInt(System.getProperty("db.idle.min")));
        ds.setMaxIdle(Integer.parseInt(System.getProperty("db.idle.max")));
        ds.setMaxOpenPreparedStatements(Integer.parseInt(System.getProperty("db.openPreparedStatement.max")));
        ds.setDriverClassName("com.mysql.cj.jdbc.Driver");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static Connection getConnection() throws SQLException {
		return ds.getConnection();
	}

}

package com.projectlibri.jdo;

public class BookDO {
	
	private int id;
	private int authorId;
	private String title;
	private String authorFirstName;
	private String authorLastName;
	
	
	public BookDO(int id, int authorId, String title, String authorFirstName, String authorLastName) {
		super();
		this.id = id;
		this.title = title;
		this.authorFirstName = authorFirstName;
		this.authorLastName = authorLastName;
		this.authorId = authorId;
	}
	
	
	public BookDO(int id, int authorId, String title) {
		super();
		this.id = id;
		this.title = title;
		this.authorId = authorId;
	}
	
	
	public int getId() {
		return id;
	}
	
	
	public void setId(int id) {
		this.id = id;
	}
	
	
	public String getTitle() {
		return title;
	}
	
	
	public void setTitle(String title) {
		this.title = title;
	}
	
	
	public String getAuthorFirstName() {
		return authorFirstName;
	}
	
	
	public void setAuthorFirstName(String authorFirstName) {
		this.authorFirstName = authorFirstName;
	}
	
	
	public String getAuthorLastName() {
		return authorLastName;
	}
	
	
	public void setAuthorLastName(String authorLastName) {
		this.authorLastName = authorLastName;
	}


	public int getAuthorId() {
		return authorId;
	}


	public void setAuthorId(int authorId) {
		this.authorId = authorId;
	}


	@Override
	public boolean equals(Object obj) {
		// TODO Auto-generated method stub
		BookDO bookObj = (BookDO) obj;
		return this.id == bookObj.getId();
	}
	
	
	

}

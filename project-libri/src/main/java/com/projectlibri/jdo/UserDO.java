package com.projectlibri.jdo;

public class UserDO {

	private int id;
	private String username;
	private String emailAddress;
	private int userTypeId;
	
	
	public UserDO(int id, String username, String emailAddress, int userTypeId) {
		super();
		this.id = id;
		this.username = username;
		this.emailAddress = emailAddress;
		this.userTypeId = userTypeId;
	}
	
	
	public int getId() {
		return id;
	}
	
	
	public String getUsername() {
		return username;
	}
	
	
	public String getEmailAddress() {
		return emailAddress;
	}
	
	
	public int getUserTypeId() {
		return userTypeId;
	}
	
	
	
}

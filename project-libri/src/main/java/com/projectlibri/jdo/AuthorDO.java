package com.projectlibri.jdo;

public class AuthorDO {
	
	private int id;
	private String firstName;
	private String lastName;
	private String bio;
	
	public AuthorDO(int id, String firstName, String lastName, String bio) {
		this.id = id;
		this.firstName = firstName;
		this.bio = bio;
		this.lastName = lastName;
	}
	
	public AuthorDO(int id, String firstName, String lastName) {
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
	}
	
	
	public String getBio() {
		return bio;
	}


	public void setBio(String bio) {
		this.bio = bio;
	}


	public int getId() {
		return id;
	}
	
	
	public void setId(int id) {
		this.id = id;
	}
	
	
	public String getFirstName() {
		return firstName;
	}
	
	
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	
	
	public String getLastName() {
		return lastName;
	}
	
	
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}


	@Override
	public boolean equals(Object obj) {
		// TODO Auto-generated method stub
		AuthorDO authorObj = (AuthorDO) obj;
		return this.id == authorObj.getId();
	}
	
	
	@Override
	public String toString() {
		return this.lastName + ", " + firstName;
	}
}

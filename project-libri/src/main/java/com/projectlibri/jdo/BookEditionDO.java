package com.projectlibri.jdo;

public class BookEditionDO {

	private int id;
	private int titleId;
	private int authorId;
	private int languageId;
	private String authorFirstName;
	private String authorLastName;
	private String language;
	private String title;
	private String languageTitle;
	private String publisher;
	private int publicationYear;
	private String isbn;
	private int year;
	private String pdfName;
	private String pdfLocation;
	private String pdfAbsoluteLocation;
	
	
	public int getId() {
		return id;
	}
	
	
	public void setId(int id) {
		this.id = id;
	}
	
	
	public int getTitleId() {
		return titleId;
	}
	
	
	public void setTitleId(int titleId) {
		this.titleId = titleId;
	}
	
	
	public int getAuthorId() {
		return authorId;
	}
	
	
	public void setAuthorId(int authorId) {
		this.authorId = authorId;
	}
	
	
	public int getLanguageId() {
		return languageId;
	}
	
	
	public void setLanguageId(int languageId) {
		this.languageId = languageId;
	}
	
	
	public String getAuthorFirstName() {
		return authorFirstName;
	}
	
	
	public void setAuthorFirstName(String authorFirstName) {
		this.authorFirstName = authorFirstName;
	}
	
	
	public String getAuthorLastName() {
		return authorLastName;
	}
	
	
	public void setAuthorLastName(String authorLastName) {
		this.authorLastName = authorLastName;
	}
	
	
	public String getLanguage() {
		return language;
	}
	
	
	public void setLanguage(String language) {
		this.language = language;
	}
	
	
	public String getTitle() {
		return title;
	}
	
	
	public void setTitle(String title) {
		this.title = title;
	}
	
	
	public String getLanguageTitle() {
		return languageTitle;
	}


	public void setLanguageTitle(String languageTitle) {
		this.languageTitle = languageTitle;
	}


	public String getPublisher() {
		return publisher;
	}
	
	
	public void setPublisher(String publisher) {
		this.publisher = publisher;
	}
	
	
	public int getPublicationYear() {
		return publicationYear;
	}
	
	
	public void setPublicationYear(int publicationYear) {
		this.publicationYear = publicationYear;
	}
	
	
	public String getIsbn() {
		return isbn;
	}
	
	
	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}


	public int getYear() {
		return year;
	}


	public void setYear(int year) {
		this.year = year;
	}


	public String getPdfName() {
		return pdfName;
	}


	public void setPdfName(String pdfName) {
		this.pdfName = pdfName;
		if (pdfName != null) {
			this.pdfLocation = System.getProperty("pdf.data.directory") + pdfName + ".pdf";
			this.pdfAbsoluteLocation = System.getProperty("pdf.data.absolue.directory") + pdfName + ".pdf";
		} else {
			this.pdfLocation = null;
			this.pdfAbsoluteLocation = null;
		}
	}


	public String getPdfLocation() {
		return pdfLocation;
	}


	public void setPdfLocation(String pdfLocation) {
		this.pdfLocation = pdfLocation;
	}


	public String getPdfAbsoluteLocation() {
		return pdfAbsoluteLocation;
	}


	public void setPdfAbsoluteLocation(String pdfAbsoluteLocation) {
		this.pdfAbsoluteLocation = pdfAbsoluteLocation;
	}
	
	
	
}

package com.projectlibri.login;

import java.io.IOException;
import java.util.AbstractMap.SimpleEntry;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class RegisterServlet
 */
@WebServlet(urlPatterns =  "/register")
public class RegisterServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public RegisterServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.getRequestDispatcher("/views/register.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String username = request.getParameter("username");
		String mailAddress = request.getParameter("mailAddress");
		String password = request.getParameter("password");
		String password2 = request.getParameter("password2");
		String firstName = request.getParameter("firstName");
		String lastName = request.getParameter("lastName");
		int userTypeId;
		boolean isUserAdminCreated = request.getParameter("isCreatedByAdmin") != null;
		
		if (request.getParameter("userTypeSelect") != null) {
			userTypeId = Integer.parseInt(request.getParameter("userTypeSelect"));
		} else {
			userTypeId = 2;
		}
		
		SimpleEntry<Boolean, String> registrationResult;
		
		if (password.equals(password2)) {
			registrationResult = new ValidationService().registerUser(mailAddress, username, password2, userTypeId, firstName, lastName);
		} else {
			registrationResult = new SimpleEntry<Boolean, String>(false, "Password don't match");
		}
		
		if (registrationResult.getKey()) {
			if (isUserAdminCreated) {
				response.sendRedirect("views/index.jsp");
			} else {
				response.sendRedirect("views/login.jsp");
			}
		} else {
			if (isUserAdminCreated) {
				request.setAttribute("errorMessage", registrationResult.getValue());
				request.getRequestDispatcher("/views/createUser.jsp").forward(request, response);
			} else {
				request.setAttribute("errorMessage", registrationResult.getValue());
				request.getRequestDispatcher("/views/register.jsp").forward(request, response);
			}
		}
	}

}

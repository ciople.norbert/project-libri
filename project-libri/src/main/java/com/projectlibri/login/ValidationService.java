package com.projectlibri.login;

import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.KeySpec;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.AbstractMap.SimpleEntry;

import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;

import org.apache.commons.codec.binary.Hex;

import com.projectlibri.db.DBPCDataSource;

public class ValidationService {
	
	public boolean isUserValid(String user, String password) {
		int numRowsFound = 0;
		
		try {
			Connection con = DBPCDataSource.getConnection();
			
			String sql = "SELECT COUNT(*) FROM project_libri.pl_users "
					+ "WHERE (username = ? or email = ?) and password = ?";
			
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setString(1, user);
			ps.setString(2, user);
			ps.setString(3, generatePasswordHash(password));
			
			ResultSet rs = ps.executeQuery();
			
			rs.next();
			
			numRowsFound = rs.getInt(1);
			
			rs.close(); 
			ps.close();
			con.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return numRowsFound > 0;
	}
	
	
	public SimpleEntry<Boolean, String> registerUser(String emailAddress, String username, String password, int userTypeId, 
			String firstName, String lastName) {
		if (isUsernameTaken(username)) {
			return new SimpleEntry<Boolean, String>(false, "Username already taken");
		}
		
		if (isMailTaken(emailAddress)) {
			return new SimpleEntry<Boolean, String>(false, "Mail address already taken");
		}
		
		if (password.length() < 7) {
			return new SimpleEntry<Boolean, String>(false, "Please use a password that is at least 7 characters long");
		}
		
		String passwordHash = generatePasswordHash(password);
		
		String sql = "INSERT INTO project_libri.pl_users "
				+ "(username, email, password, user_type_id, first_name, last_name) "
				+ "VALUES (?, ?, ?, ?, ?, ?)";
		
		Connection con;
		try {
			con = DBPCDataSource.getConnection();
			
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setString(1, username);
			ps.setString(2, emailAddress);
			ps.setString(3, passwordHash);
			ps.setInt(4, userTypeId);
			ps.setString(5, firstName);
			ps.setString(6, lastName);
			
			ps.executeUpdate();
			
			ps.close();
			con.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return new SimpleEntry<Boolean, String>(true, "Account created successfully");
	}
	
	
	private boolean isUsernameTaken(String username) {
		int numRowsFound = 0;
		
		try {
			Connection con = DBPCDataSource.getConnection();
			
			String sql = "SELECT COUNT(*) FROM project_libri.pl_users WHERE username = ?";
			
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setString(1, username);
			
			ResultSet rs = ps.executeQuery();
			
			rs.next();
			
			numRowsFound = rs.getInt(1);
			
			rs.close();
			ps.close();
			con.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return numRowsFound > 0;
	}
	
	
	private boolean isMailTaken(String mailAddress) {
		int numRowsFound = 0;
		
		try {
			Connection con = DBPCDataSource.getConnection();
			
			String sql = "SELECT COUNT(*) FROM project_libri.pl_users WHERE email = ?";
			
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setString(1, mailAddress);
			
			ResultSet rs = ps.executeQuery();
			
			rs.next();
			
			numRowsFound = rs.getInt(1);
			
			rs.close();
			ps.close();
			con.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return numRowsFound > 0;
	}
	
	
	public String generatePasswordHash(String password) {
		String salt = System.getProperty("security.pbkdf2.salt");
		int iterations = Integer.parseInt(System.getProperty("security.pbkdf2.iterations"));
		int keyLength = Integer.parseInt(System.getProperty("security.pbkdf2.keylength"));
		
        char[] passwordChars = password.toCharArray();
        byte[] saltBytes = salt.getBytes();
        
		String hashedPassword = null;
        
        SecretKeyFactory skf;
		try {
			skf = SecretKeyFactory.getInstance( "PBKDF2WithHmacSHA512" );
			PBEKeySpec spec = new PBEKeySpec(passwordChars, saltBytes, iterations, keyLength );
	        SecretKey key = skf.generateSecret( spec );
	        byte[] res = key.getEncoded( );
	        
	        hashedPassword = Hex.encodeHexString(res);
	        
		} catch (Exception e) {
			e.printStackTrace();
		}
		

		return hashedPassword;
	}
	
	
	public SimpleEntry<Boolean, String> changePassword(String username, String oldPassword, String newPassword) {
		if (!isUserValid(username, oldPassword)) {
			return new SimpleEntry<Boolean, String>(false, "Incorrect old password");
		}
		
		if (newPassword.length() < 7) {
			return new SimpleEntry<Boolean, String>(false, "Please use a password that is at least 7 characters long");
		}
		
		String passwordHash = generatePasswordHash(newPassword);
		
		String sql = "UPDATE project_libri.pl_users SET password = ? WHERE email = ?";
		
		Connection con;
		try {
			con = DBPCDataSource.getConnection();
			
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setString(2, username);
			ps.setString(1, passwordHash);
		
			
			ps.executeUpdate();
			
			ps.close();
			con.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return new SimpleEntry<Boolean, String>(true, "Account created successfully");
		
	}

}

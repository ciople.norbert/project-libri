package com.projectlibri.ocr;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Timestamp;

import javax.servlet.AsyncContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebFilter;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.projectlibri.utils.DBUtils;

import net.sourceforge.tess4j.Tesseract;
import net.sourceforge.tess4j.TesseractException;

/**
 * Servlet implementation class OCRServlet
 */
@WebServlet(urlPatterns = "/transcribeAction", asyncSupported=true)
public class OCRServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public OCRServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub

//		new Thread() {
//			
//			@Override
//			public void run() {
//				
//			}
//		}.start();
		
		String fileLocation = request.getParameter("pdfFile");
		int languageId = Integer.parseInt(request.getParameter("languageId"));
		
		OCRHandler ocr = new OCRHandler();
		String extractedText = ocr.getTextFromFile(fileLocation, languageId);

//		Timestamp timestamp = new Timestamp(System.currentTimeMillis());
//		String tempFileName = timestamp.toString().replace(" " , "-").replace(":", "-");
//		File saveFile = new File(System.getProperty("temporary.transcriptions.absolute.directory") + tempFileName + ".txt");
//		
//		System.out.println(saveFile.getPath());
//		
//		saveFile.createNewFile();
//		PrintWriter out = new PrintWriter(saveFile);
//		out.print(extractedText);
//		out.close();
		
		int bookId = Integer.parseInt(request.getParameter("bookId"));
		
		DBUtils.saveExtractedText(bookId, languageId, extractedText);
		
		String editorRedirectAddress =  "views/reader.jsp?bookId=" + request.getParameter("bookId") + 
				"&languageId=" + languageId; 
		
//		request.getRequestDispatcher("/views/editor.jsp").forward(request, response);
		response.sendRedirect(editorRedirectAddress);
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
//		doGet(request, response);
	}

}

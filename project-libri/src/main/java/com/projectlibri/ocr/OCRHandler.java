package com.projectlibri.ocr;

import java.io.File;

import com.projectlibri.utils.DBUtils;

import net.sourceforge.tess4j.Tesseract;
import net.sourceforge.tess4j.TesseractException;

public class OCRHandler {
	
	public String getTextFromFile(String fileSource, int languageId) {
		File image = new File(fileSource);
		
		String language = DBUtils.getTessLangForId(languageId);
		
		if (language == null || useEnglishOnly()) {
			language = "eng";
		}
		
		System.out.println("Translation language: " + language);
		
		Tesseract tesseract = new Tesseract();
		tesseract.setDatapath(System.getProperty("tessaract.training.data.directory"));
		tesseract.setLanguage(language);
		tesseract.setPageSegMode(1);
		tesseract.setOcrEngineMode(1);
		String result = null;
		try {
			result = tesseract.doOCR(image);
			System.out.println(result);
		} catch (TesseractException e) {
			e.printStackTrace();
		}
		
		return result;
	}
	
	
	private boolean useEnglishOnly() {
		String englishOnlyProp = System.getProperty("tessaract.use.eng.only"); 
		return !"0".equals(englishOnlyProp);  
	}

}

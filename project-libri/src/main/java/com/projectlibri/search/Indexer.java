package com.projectlibri.search;

import java.io.File;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.util.Version;

import com.projectlibri.db.DBPCDataSource;

public class Indexer {

	private static final String indexDirectory = System.getProperty("lucene.index.directory");
	
	
	public void generateIndex() {
		File indexDir = new File(indexDirectory);
		Analyzer analyzer = new StandardAnalyzer(Version.LUCENE_36);
		IndexWriterConfig config = new IndexWriterConfig(Version.LUCENE_36, analyzer);
		config.setOpenMode(IndexWriterConfig.OpenMode.CREATE);
		try {
			IndexWriter indexWriter = new IndexWriter(FSDirectory.open(indexDir), config);
			
			Connection con = DBPCDataSource.getConnection();
			
			String sql = "SELECT books.*, authors.first_name, authors.last_name, authors.id AS author_id FROM "
					+ "(SELECT editions.id, editions.isbn, titles.title, titles.id AS title_id, "
					+ "titles.author_id AS title_author_id, editions.language_title, editions.language_id "
					+ "FROM project_libri.ct_books_editions AS editions "
					+ "INNER JOIN project_libri.ct_titles AS titles ON titles.id = editions.title_id) AS books "
					+ "RIGHT JOIN project_libri.ct_authors AS authors ON authors.id = books.title_author_id ";
//					+ "GROUP BY books.title, books.title_id;";
			
			PreparedStatement ps = con.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			
			while (rs.next()) {
				Document doc = new Document();
				String dbValue = rs.getString("title_id");
				doc.add(new Field("id", dbValue != null ? dbValue : "-1", Field.Store.YES, Field.Index.NOT_ANALYZED));
				doc.add(new Field("author_id", rs.getString("author_id"), Field.Store.YES, Field.Index.NOT_ANALYZED));
				dbValue = rs.getString("isbn");
				doc.add(new Field("isbn", dbValue != null ? dbValue : "null", Field.Store.NO, Field.Index.ANALYZED));
				dbValue = rs.getString("title");
				doc.add(new Field("title", dbValue != null ? dbValue : "null", Field.Store.YES, Field.Index.ANALYZED));
				doc.add(new Field("firstname", rs.getString("first_name"), Field.Store.YES, Field.Index.ANALYZED));
				doc.add(new Field("lastname", rs.getString("last_name"), Field.Store.YES, Field.Index.ANALYZED));
				dbValue = rs.getString("language_title");
				doc.add(new Field("language_title", dbValue != null ? dbValue : "null", Field.Store.NO, Field.Index.ANALYZED));
				indexWriter.addDocument(doc);
				System.out.println("Indexed entry with id: " + rs.getString("id"));
			}
			
			indexWriter.close();
			
			rs.close();
			ps.close();
			con.close();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	public void addAuthorToIndex(int id) {
		File indexDir = new File(indexDirectory);
		Analyzer analyzer = new StandardAnalyzer(Version.LUCENE_36);
		IndexWriterConfig config = new IndexWriterConfig(Version.LUCENE_36, analyzer);
		config.setOpenMode(IndexWriterConfig.OpenMode.APPEND);
		try {
			IndexWriter indexWriter = new IndexWriter(FSDirectory.open(indexDir), config);
			
			Connection con = DBPCDataSource.getConnection();
			
			String sql = "SELECT books.*, authors.first_name, authors.last_name, authors.id AS author_id FROM "
					+ "(SELECT editions.id, editions.isbn, titles.title, titles.id AS title_id, "
					+ "titles.author_id AS title_author_id, editions.language_title, editions.language_id "
					+ "FROM project_libri.ct_books_editions AS editions "
					+ "INNER JOIN project_libri.ct_titles AS titles ON titles.id = editions.title_id) AS books "
					+ "RIGHT JOIN project_libri.ct_authors AS authors ON authors.id = books.title_author_id "
					+ "WHERE authors.id = ?;";
			
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setInt(1, id);
			ResultSet rs = ps.executeQuery();
			
			while (rs.next()) {
				Document doc = new Document();
				String dbValue = rs.getString("title_id");
				doc.add(new Field("id", dbValue != null ? dbValue : "-1", Field.Store.YES, Field.Index.NOT_ANALYZED));
				doc.add(new Field("author_id", rs.getString("author_id"), Field.Store.YES, Field.Index.NOT_ANALYZED));
				dbValue = rs.getString("isbn");
				doc.add(new Field("isbn", dbValue != null ? dbValue : "null", Field.Store.NO, Field.Index.ANALYZED));
				dbValue = rs.getString("title");
				doc.add(new Field("title", dbValue != null ? dbValue : "null", Field.Store.YES, Field.Index.ANALYZED));
				doc.add(new Field("firstname", rs.getString("first_name"), Field.Store.YES, Field.Index.ANALYZED));
				doc.add(new Field("lastname", rs.getString("last_name"), Field.Store.YES, Field.Index.ANALYZED));
				dbValue = rs.getString("language_title");
				doc.add(new Field("language_title", dbValue != null ? dbValue : "null", Field.Store.NO, Field.Index.ANALYZED));
				indexWriter.addDocument(doc);
				System.out.println("Indexed entry with id: " + rs.getString("id"));
			}
			
			indexWriter.close();
			
			rs.close();
			ps.close();
			con.close();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	public void addTitleToIndex(int id) {
		File indexDir = new File(indexDirectory);
		Analyzer analyzer = new StandardAnalyzer(Version.LUCENE_36);
		IndexWriterConfig config = new IndexWriterConfig(Version.LUCENE_36, analyzer);
		config.setOpenMode(IndexWriterConfig.OpenMode.APPEND);
		try {
			IndexWriter indexWriter = new IndexWriter(FSDirectory.open(indexDir), config);
			
			Connection con = DBPCDataSource.getConnection();
			
			String sql = "SELECT books.*, authors.first_name, authors.last_name, authors.id AS author_id FROM "
					+ "(SELECT editions.id, editions.isbn, titles.title, titles.id AS title_id, "
					+ "titles.author_id AS title_author_id, editions.language_title, editions.language_id "
					+ "FROM project_libri.ct_books_editions AS editions "
					+ "INNER JOIN project_libri.ct_titles AS titles ON titles.id = editions.title_id) AS books "
					+ "RIGHT JOIN project_libri.ct_authors AS authors ON authors.id = books.title_author_id "
					+ "WHERE books.id = ?;";
			
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setInt(1, id);
			ResultSet rs = ps.executeQuery();
			
			while (rs.next()) {
				Document doc = new Document();
				String dbValue = rs.getString("title_id");
				doc.add(new Field("id", dbValue != null ? dbValue : "-1", Field.Store.YES, Field.Index.NOT_ANALYZED));
				doc.add(new Field("author_id", rs.getString("author_id"), Field.Store.YES, Field.Index.NOT_ANALYZED));
				dbValue = rs.getString("isbn");
				doc.add(new Field("isbn", dbValue != null ? dbValue : "null", Field.Store.NO, Field.Index.ANALYZED));
				dbValue = rs.getString("title");
				doc.add(new Field("title", dbValue != null ? dbValue : "null", Field.Store.YES, Field.Index.ANALYZED));
				doc.add(new Field("firstname", rs.getString("first_name"), Field.Store.YES, Field.Index.ANALYZED));
				doc.add(new Field("lastname", rs.getString("last_name"), Field.Store.YES, Field.Index.ANALYZED));
				dbValue = rs.getString("language_title");
				doc.add(new Field("language_title", dbValue != null ? dbValue : "null", Field.Store.NO, Field.Index.ANALYZED));
				indexWriter.addDocument(doc);
				System.out.println("Indexed entry with id: " + rs.getString("id"));
			}
			
			indexWriter.close();
			
			rs.close();
			ps.close();
			con.close();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
}

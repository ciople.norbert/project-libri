package com.projectlibri.search;

import java.io.File;
import java.util.ArrayList;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.Term;
import org.apache.lucene.search.BooleanClause;
import org.apache.lucene.search.BooleanQuery;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.MatchAllDocsQuery;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TermQuery;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.util.Version;

import com.projectlibri.jdo.BookDO;



public class Searcher {

	private static final String indexDirectory = System.getProperty("lucene.index.directory");
	private static final int maxHits = 100;
	
	public ArrayList<BookDO> searchIndex(String queryStr) {
		ArrayList<BookDO> resultBooks = new ArrayList<BookDO>();
		
		try {
			File indexDir = new File(indexDirectory);
			Directory directory = FSDirectory.open(indexDir);
			IndexSearcher indexSearcher = new IndexSearcher(directory);
			Analyzer analyzer = new StandardAnalyzer(Version.LUCENE_36);

			String[] searchQueryTerms = queryStr.split(" ");
			BooleanQuery query = new BooleanQuery();
			
			for (String term: searchQueryTerms) {
				query.add(new TermQuery(new Term("title", term)), BooleanClause.Occur.SHOULD);
				query.add(new TermQuery(new Term("firstname", term)), BooleanClause.Occur.SHOULD);
				query.add(new TermQuery(new Term("lastname", term)), BooleanClause.Occur.SHOULD);
				query.add(new TermQuery(new Term("isbn", term)), BooleanClause.Occur.SHOULD);
				query.add(new TermQuery(new Term("language_title", term)), BooleanClause.Occur.SHOULD);
			}

			System.out.println(query);
			TopDocs topDocs = indexSearcher.search(query, maxHits);
			ScoreDoc[] hits = topDocs.scoreDocs;
			
			for (int i = 0; i < hits.length; i++) {
				int docId = hits[i].doc;
				Document d = indexSearcher.doc(docId);
				
				int resultId = Integer.parseInt(d.get("id"));
				int resultAuthorId = Integer.parseInt(d.get("author_id"));
				String resultTitle = d.get("title");
				String resultFirstName = d.get("firstname");
				String resultLastName = d.get("lastname");
				
				System.out.println(resultId + ": " + resultTitle + "; d.Score: " + hits[i].score);
				
				resultBooks.add(new BookDO(resultId, resultAuthorId, resultTitle, resultFirstName, resultLastName));
			}
			
			indexSearcher.close();
			analyzer.close();
			System.out.println("Found: " + hits.length);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return resultBooks;
	}
	
	
	public ArrayList<BookDO> getAllDocs() {
		ArrayList<BookDO> results = new ArrayList<BookDO>();
		
		try {
			File indexDir = new File(indexDirectory);
			Directory directory = FSDirectory.open(indexDir);
			IndexSearcher indexSearcher = new IndexSearcher(directory);
			
			Query query = new MatchAllDocsQuery();
			TopDocs topDocs = indexSearcher.search(query, Integer.MAX_VALUE);
			ScoreDoc[] hits = topDocs.scoreDocs;
			
			for (int i = 0; i < hits.length; i++) {
				int docId = hits[i].doc;
				Document d = indexSearcher.doc(docId);
				
				int resultId = Integer.parseInt(d.get("id"));
				int resultAuthorId = Integer.parseInt(d.get("author_id"));
				String resultTitle = d.get("title");
				String resultFirstName = d.get("firstname");
				String resultLastName = d.get("lastname");
				
				System.out.println(resultId + ": " + resultTitle + "; d.Score: " + hits[i].score);
				
				results.add(new BookDO(resultId, resultAuthorId, resultTitle, resultFirstName, resultLastName));
			}
			
			indexSearcher.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return results;
	}
	
}

package com.projectlibri.account;

import java.io.IOException;
import java.util.AbstractMap.SimpleEntry;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.projectlibri.jdo.UserDO;
import com.projectlibri.login.ValidationService;

/**
 * Servlet implementation class LogoutServlet
 */
@WebServlet(urlPatterns = "/accountAction")
public class AccountActionServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AccountActionServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		HttpSession session = request.getSession();
		
		String action = request.getParameter("action");
		
		if ("logout".equals(action)) {
			session.invalidate();			
			response.sendRedirect("/project-libri/login");
		} else if ("details".equals(action)) {
			response.sendRedirect("views/accountDetails.jsp");
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		HttpSession session = request.getSession();
		
		UserDO userData = (UserDO) session.getAttribute("userData");
		
		if (userData == null) {
			request.getRequestDispatcher("/views/login.jsp").forward(request, response);
			return;
		}
		
		String userMail = userData.getEmailAddress();
		String oldPassword = request.getParameter("oldPassword");
		String newPassword1 = request.getParameter("newPassword1");
		String newPassword2 = request.getParameter("newPassword2");
		
		SimpleEntry<Boolean, String> passwordChangeResult;
		
		if (newPassword1.equals(newPassword2)) {
			passwordChangeResult = new ValidationService().changePassword(userMail, oldPassword, newPassword1);
		} else {
			passwordChangeResult = new SimpleEntry<Boolean, String>(false, "Password don't match");
		}
		
		if (passwordChangeResult.getKey()) {
			response.sendRedirect("views/accountDetails.jsp");
		} else {
			
			request.setAttribute("errorMessage", passwordChangeResult.getValue());
			request.getRequestDispatcher("/views/changePassword.jsp").forward(request, response);
		}
		
	}

}

package com.projectlibri.book;

import java.io.File;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import com.projectlibri.jdo.AuthorDO;
import com.projectlibri.jdo.BookDO;
import com.projectlibri.jdo.BookEditionDO;
import com.projectlibri.jdo.LanguageDO;
import com.projectlibri.search.Indexer;
import com.projectlibri.utils.DBUtils;
import com.projectlibri.utils.FilesUtils;

/**
 * Servlet implementation class BookServlet
 */
@WebServlet("/bookAction")
@MultipartConfig(fileSizeThreshold = 1024 * 1024,
maxFileSize = 1024 * 1024 * 5, 
maxRequestSize = 1024 * 1024 * 5 * 5)
public class BookServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public BookServlet() {
        super();
        // TODO Auto-generated constructor stub
    }
   

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String action = request.getParameter("action");
		
		System.out.println("Action: " + action);
		
		String uploadPath = System.getProperty("pdf.data.absolue.directory");
		
		System.out.println("Upload path: " + uploadPath);
		
		if ("add".equals(action)) {
			boolean useExistingTitle = request.getParameter("existingTitle") != null;
			int authorId = Integer.parseInt(request.getParameter("authorId"));
			int titleId = -1;
			if (useExistingTitle) {
				titleId = Integer.parseInt(request.getParameter("titleId"));
			}
			String title = request.getParameter("titleInput");
			boolean updateIndex = request.getParameter("updateIndex") != null;
			int languageId = Integer.parseInt(request.getParameter("languageSelect"));
			boolean useSameTitle = request.getParameter("useSameTitle") != null;
			String languageTitle = request.getParameter("languageTitleInput");
			if (useSameTitle) {
				BookDO titleObj = DBUtils.getTitleForId(titleId);
				if (titleObj != null) {
					languageTitle = titleObj.getTitle();
				} else {
					languageTitle = title;
				}
			}
			String publisher = request.getParameter("publisherInput");
			int publishingYear = Integer.parseInt(request.getParameter("publicationYearInput"));
			int originalPublishingYear = -1; 
			if (!useExistingTitle) {
				originalPublishingYear = Integer.parseInt(request.getParameter("originalPublicationYearInput"));
			}
			String isbn = request.getParameter("isbnInput");
			
			String fileName = "";
			
			Part part = request.getPart("fileUpload");
		    if (!part.getSubmittedFileName().equals("")) {
				fileName = FilesUtils.generateFileName(authorId, titleId, title, languageId);
			    part.write(uploadPath + File.separator + fileName + ".pdf");
		    }
			
			if (titleId == -1) {
				titleId = DBUtils.addTitle(authorId, title, languageId, originalPublishingYear);
			}
			
			if (DBUtils.getBookEdition(titleId, languageId) == null) {
				BookEditionDO book = new BookEditionDO();
				book.setAuthorId(authorId);
				book.setTitleId(titleId);
				book.setLanguageId(languageId);
				book.setIsbn(isbn);
				book.setPublicationYear(publishingYear);
				book.setPublisher(publisher);
				book.setLanguageTitle(languageTitle);
				
				int editionId = DBUtils.addBookEdition(book);
				
				if (updateIndex) {
					new Indexer().addTitleToIndex(editionId);
				}
			}
			
			if (!fileName.equals("")) {
				DBUtils.setPdfForBook(titleId, languageId, fileName);
			}

			
		} else if ("uploadPdf".equals(action)) {
			try {
				System.out.println("File upload started");
				String fileName = "";
				Part part = request.getPart("fileUpload");
				int authorId = Integer.parseInt(request.getParameter("authorId"));
				int titleId = Integer.parseInt(request.getParameter("titleId"));
				int languageId = Integer.parseInt(request.getParameter("languageId"));
				System.out.println("Parmeters received");
				
				if (!part.getSubmittedFileName().equals("")) {
					fileName = FilesUtils.generateFileName(authorId, titleId, null, languageId);
				    part.write(uploadPath + File.separator + fileName + ".pdf");
				    System.out.println("Part written");
			    }
				
				if (!fileName.equals("")) {
					DBUtils.setPdfForBook(titleId, languageId, fileName);
					System.out.println("PDF added to DB");
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			
		}
		
		response.sendRedirect("views/index.jsp");
	}

}

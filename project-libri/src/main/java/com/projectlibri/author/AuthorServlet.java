package com.projectlibri.author;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.projectlibri.search.Indexer;
import com.projectlibri.utils.DBUtils;

/**
 * Servlet implementation class AuthorSerlvet
 */
@WebServlet("/authorAction")
public class AuthorServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AuthorServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String action = request.getParameter("action");
		
		if ("add".equals(action)) {
			String firstName = request.getParameter("firstName");
			String lastName = request.getParameter("lastName");
			String bio = request.getParameter("bio");
			boolean updateIndex = request.getParameter("updateIndex") != null;
			
			int insertId = DBUtils.addAuthor(firstName, lastName, bio);
			
			System.out.println("INSERT ID:" + insertId);
			
			if (updateIndex && insertId != -1) {
				new Indexer().addAuthorToIndex(insertId);
			}
			
		}
		
		response.sendRedirect("views/index.jsp");
	}

}

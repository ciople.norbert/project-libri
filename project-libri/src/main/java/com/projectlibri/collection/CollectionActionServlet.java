package com.projectlibri.collection;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.projectlibri.utils.DBUtils;

/**
 * Servlet implementation class addBookToCollectionServlet
 */
@WebServlet("/collectionAction")
public class CollectionActionServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public CollectionActionServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
//		response.getWriter().append("Served at: ").append(request.getContextPath());
		int bookId = Integer.parseInt(request.getParameter("bookId"));
		int userId = Integer.parseInt(request.getParameter("userId"));
		int languageId = Integer.parseInt(request.getParameter("languageId"));
		String action = request.getParameter("action");
		
		if ("add".equals(action) && !DBUtils.isBookInUserCollection(bookId, languageId, userId)) {
			DBUtils.addBookToUserCollection(bookId, languageId, userId);
		} else if ("remove".equals(action) && DBUtils.isBookInUserCollection(bookId, languageId, userId)) {
			DBUtils.removeBookFromCollection(bookId, languageId, userId);
		}
		
		response.sendRedirect("views/bookDetails.jsp?bookId=" + bookId + "&languageId=" + languageId);
	}

}

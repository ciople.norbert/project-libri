package com.projectlibri.utils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.Types;
import java.util.AbstractMap.SimpleEntry;
import java.util.ArrayList;
import java.util.HashMap;

import com.projectlibri.db.DBPCDataSource;
import com.projectlibri.jdo.AuthorDO;
import com.projectlibri.jdo.BookDO;
import com.projectlibri.jdo.BookEditionDO;
import com.projectlibri.jdo.LanguageDO;
import com.projectlibri.jdo.UserTypeDO;

public class DBUtils {
	
	
	public static BookEditionDO getBookEdition(int titleId, int languageId) {
		BookEditionDO bookEdition = null;
		
		String sql = "SELECT editions.*, authors.first_name, authors.last_name, languages.`language`, "
				+ "titles.title, titles.`year` FROM project_libri.ct_books_editions AS editions "
				+ "INNER JOIN project_libri.ct_authors AS authors ON authors.id = editions.author_id "
				+ "INNER JOIN project_libri.ct_titles AS titles ON titles.id = editions.title_id "
				+ "INNER JOIN project_libri.ct_languages AS languages ON languages.id = editions.language_id "
				+ "WHERE title_id = ? AND language_id = ?;";
		
		try {
			Connection con = DBPCDataSource.getConnection();
			PreparedStatement ps = con.prepareStatement(sql);
			
			ps.setInt(1, titleId);
			ps.setInt(2, languageId);
			
			System.out.println(ps);
			
			ResultSet rs = ps.executeQuery();
			
			if (rs.isBeforeFirst()){
				rs.next();
				bookEdition = new BookEditionDO();
				bookEdition.setId(rs.getInt("id"));
				bookEdition.setAuthorId(rs.getInt("author_id"));
				bookEdition.setTitleId(rs.getInt("title_id"));
				bookEdition.setLanguageId(rs.getInt("language_id"));
				bookEdition.setIsbn(rs.getString("isbn"));
				bookEdition.setPublicationYear(rs.getInt("publication_year"));
				bookEdition.setPublisher(rs.getString("publisher"));
				bookEdition.setLanguageTitle(rs.getString("language_title"));
				bookEdition.setAuthorFirstName(rs.getString("first_name"));
				bookEdition.setAuthorLastName(rs.getString("last_name"));
				bookEdition.setLanguage(rs.getString("language"));
				bookEdition.setTitle(rs.getString("title"));
				bookEdition.setYear(rs.getInt("year"));
				bookEdition.setPdfName(rs.getString("pdf_name"));
			}
			
			
			rs.close();
			ps.close();
			con.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return bookEdition;
	}
	
	
	public static int getDefaultLanguageId(int titleId) {
		int languageId = -1;
		
		String sql = "SELECT default_language_id FROM project_libri.ct_titles WHERE id = ?";
		
		try {
			Connection con = DBPCDataSource.getConnection();
			PreparedStatement ps = con.prepareStatement(sql);
			
			ps.setInt(1, titleId);
			
			ResultSet rs = ps.executeQuery();
			
			if (rs.isBeforeFirst()){
				rs.next();
				languageId = rs.getInt("default_language_id");
			}
			
			rs.close();
			ps.close();
			con.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return languageId;
	}
	
	
	public static boolean titleWithIdExists(int id) {
		int rowsFound = -1;
		
		String sql = "SELECT COUNT(*) AS cnt FROM project_libri.ct_titles WHERE id = ?";
		
		try {
			Connection con = DBPCDataSource.getConnection();
			PreparedStatement ps = con.prepareStatement(sql);
			
			ps.setInt(1, id);
			
			ResultSet rs = ps.executeQuery();
			
			if (rs.isBeforeFirst()){
				rs.next();
				 rowsFound= rs.getInt("cnt");
			}
			
			rs.close();
			ps.close();
			con.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return rowsFound > 0;
	}
	
	
	public static HashMap<Integer, String> getLanguagesForTitle(int titleId) {
		HashMap<Integer, String> languages = new HashMap<Integer, String>();
		
		String sql = "SELECT * FROM project_libri.ct_languages WHERE id IN "
				+ "(SELECT language_id FROM project_libri.ct_books_editions WHERE title_id  = ?);"; 
		
		try {
			Connection con = DBPCDataSource.getConnection();
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setInt(1, titleId);
			
			ResultSet rs = ps.executeQuery();
			
			while(rs.next()) {
				languages.put(rs.getInt("id"), rs.getString("language"));
			}
			
			rs.close();
			ps.close();
			con.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return languages;
	}
	
	
	public static boolean authorWithIdExists(int id) {
		int rowsFound = -1;
		
		String sql = "SELECT COUNT(*) AS cnt FROM project_libri.ct_authors WHERE id = ?";
		
		try {
			Connection con = DBPCDataSource.getConnection();
			PreparedStatement ps = con.prepareStatement(sql);
			
			ps.setInt(1, id);
			
			ResultSet rs = ps.executeQuery();
			
			if (rs.isBeforeFirst()){
				rs.next();
				 rowsFound= rs.getInt("cnt");
			}
			
			rs.close();
			ps.close();
			con.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return rowsFound > 0;
	}
	
	
	public static AuthorDO getAuthorForId(int id) {
		AuthorDO author = null;
		
		String sql = "SELECT * FROM project_libri.ct_authors WHERE id = ?;";
		
		try {
			Connection con = DBPCDataSource.getConnection();
			PreparedStatement ps = con.prepareStatement(sql);
			
			ps.setInt(1, id);
			
			ResultSet rs = ps.executeQuery();
			
			if (rs.isBeforeFirst()) {
				rs.next();
				author = new AuthorDO(rs.getInt("id"), rs.getString("first_name"), rs.getString("last_name"), rs.getString("bio"));
			}
			
			con.close();
			rs.close();
			ps.close();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return author;
	}
	
	
	public static ArrayList<BookDO> getBooksByAuthor(AuthorDO author) {
		int authorId = author.getId();
		ArrayList<BookDO> books = new ArrayList<BookDO>();
		
		String sql = "SELECT id, title FROM project_libri.ct_titles WHERE author_id = ?;";
		 try {
			 Connection con = DBPCDataSource.getConnection();
			 PreparedStatement ps = con.prepareStatement(sql);
			 
			 ps.setInt(1, authorId);
			 
			 ResultSet rs = ps.executeQuery();
			 
			 if (rs.isBeforeFirst()) {
				 while (rs.next()) {
					 books.add(new BookDO(rs.getInt("id"), authorId, rs.getString("title"), author.getFirstName(), author.getLastName()));
				 }
			 }
			 
			 con.close();
			 rs.close();
			 ps.close();
			 
		 }catch (Exception e) {
			e.printStackTrace();
		}
		
		return books;
	}
	
	
	public static ArrayList<BookDO> getBooksByAuthor(int authorId) {
		ArrayList<BookDO> books = new ArrayList<BookDO>();
		
		String sql = "SELECT id, title FROM project_libri.ct_titles WHERE author_id = ?;";
		 try {
			 Connection con = DBPCDataSource.getConnection();
			 PreparedStatement ps = con.prepareStatement(sql);
			 
			 ps.setInt(1, authorId);
			 
			 ResultSet rs = ps.executeQuery();
			 
			 if (rs.isBeforeFirst()) {
				 while (rs.next()) {
					 books.add(new BookDO(rs.getInt("id"), authorId, rs.getString("title")));
				 }
			 }
			 
			 con.close();
			 rs.close();
			 ps.close();
			 
		 }catch (Exception e) {
			e.printStackTrace();
		}
		
		return books;
	}
	
	
	public static boolean isBookInUserCollection(int bookId, int languageId, int userId) {
		String sql = "SELECT COUNT(*) AS cnt FROM project_libri.cl_user_book_collections WHERE user_id = ? AND book_edition_id = "
				+ "(SELECT id FROM project_libri.ct_books_editions WHERE title_id = ? AND language_id = ?)";
		
		int rowsFound = -1;
		
		try {
			Connection con = DBPCDataSource.getConnection();
			PreparedStatement ps = con.prepareStatement(sql);
			
			ps.setInt(1, userId);
			ps.setInt(2, bookId);
			ps.setInt(3, languageId);
			
			ResultSet rs = ps.executeQuery();
			
			rs.next();
			
			rowsFound = rs.getInt("cnt");
			
			ps.close();
			rs.close();
			con.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return rowsFound > 0;
	}
	
	
	public static void addBookToUserCollection(int bookId, int languageId, int userId) {
		String sql = "INSERT INTO project_libri.cl_user_book_collections (user_id, book_edition_id) VALUES (?, "
				+ "(SELECT id FROM project_libri.ct_books_editions WHERE title_id = ? AND language_id = ?))";
		
		try {
			Connection con = DBPCDataSource.getConnection();
			PreparedStatement ps = con.prepareStatement(sql);
			
			ps.setInt(1, userId);
			ps.setInt(2, bookId);
			ps.setInt(3, languageId);
			
			ps.executeUpdate();
			
			ps.close();
			con.close();
		}catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	public static void removeBookFromCollection(int bookId, int languageId, int userId) {
		String sql = "DELETE FROM project_libri.cl_user_book_collections WHERE user_id = ? AND book_edition_id =  "
				+ "(SELECT id FROM project_libri.ct_books_editions WHERE title_id = ? AND language_id = ?)";
		
		try {
			Connection con = DBPCDataSource.getConnection();
			PreparedStatement ps = con.prepareStatement(sql);
			
			ps.setInt(1, userId);
			ps.setInt(2, bookId);
			ps.setInt(3, languageId);
			
			ps.executeUpdate();
			
			ps.close();
			con.close();
		}catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	public static ArrayList<BookEditionDO> getCollectionForUser(int userId) {
		ArrayList<BookEditionDO> books = new ArrayList<BookEditionDO>();
		
		String sql = "SELECT be.title_id, be.language_id, be.language_title, au.first_name, au.last_name, la.`language`, ti.author_id "
				+ "FROM project_libri.ct_books_editions AS be "
				+ "INNER JOIN project_libri.ct_languages AS la ON la.id = be.language_id "
				+ "INNER JOIN project_libri.ct_titles AS ti ON ti.id = be.title_id "
				+ "INNER JOIN project_libri.ct_authors AS au ON ti.author_id = au.id "
				+ "WHERE be.id IN (SELECT book_edition_id FROM project_libri.cl_user_book_collections WHERE user_id = ?);";
		
		try {
			Connection con = DBPCDataSource.getConnection();
			PreparedStatement ps = con.prepareStatement(sql);
			
			System.out.println(ps);
			
			ps.setInt(1, userId);
			
			ResultSet rs = ps.executeQuery();
			
			if (rs.isBeforeFirst()) {
				while (rs.next()) {
					BookEditionDO be = new BookEditionDO();
					be.setTitleId(rs.getInt("title_id"));
					be.setLanguageId(rs.getInt("language_id"));
					be.setLanguage(rs.getString("language"));
					be.setAuthorFirstName(rs.getString("first_name"));
					be.setAuthorLastName(rs.getString("last_name"));
					be.setAuthorId(rs.getInt("author_id"));
					be.setLanguageTitle(rs.getString("language_title"));
					books.add(be);
				}
			}
			
			con.close();
			rs.close();
			ps.close();
		}catch (Exception e) {
			e.printStackTrace();
		}
		
		return books;
	}
	
	
	public static void saveExtractedText(int bookId, int languageId, String text) {
		String sql; 
		if (bookHasExtractedText(bookId, languageId)) {
			sql = "UPDATE project_libri.ct_transcriptions SET extracted_text = ? WHERE title_id = ? AND language_id = ?;";
		} else {
			sql = "INSERT INTO project_libri.ct_transcriptions (extracted_text, title_id, language_id) VALUES (?, ?, ?);";
		}
		
		
		try {
			Connection con = DBPCDataSource.getConnection();
			
			PreparedStatement ps = con.prepareStatement(sql);
			
			ps.setString(1, text);
			ps.setInt(2, bookId);
			ps.setInt(3, languageId);
			
			ps.executeUpdate();
			
			ps.close();
			con.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	public static boolean bookHasExtractedText(int bookId, int languageId) {
		String sql = "SELECT COUNT(*) AS cnt FROM project_libri.ct_transcriptions WHERE title_id = ? AND language_id = ? "
				+ "AND extracted_text IS NOT NULL;";
		
		int rowsFound = -1;
		
		try {
			Connection con = DBPCDataSource.getConnection();
			PreparedStatement ps = con.prepareStatement(sql);
			
			ps.setInt(1, bookId);
			ps.setInt(2, languageId);
			
			ResultSet rs = ps.executeQuery();
			
			rs.next();
			rowsFound = rs.getInt("cnt");
			
			rs.close();
			ps.close();
			con.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return rowsFound > 0;
	}
	
	
	public static String getExtractedTextFromDB(int bookId, int languageId) {
		String sql = "SELECT extracted_text FROM project_libri.ct_transcriptions WHERE title_id = ? AND language_id = ? ";
		
		String text = null;
		
		try {
			Connection con = DBPCDataSource.getConnection();
			PreparedStatement ps = con.prepareStatement(sql);
			
			ps.setInt(1, bookId);
			ps.setInt(2, languageId);
			
			ResultSet rs = ps.executeQuery();
			
			if (!rs.isBeforeFirst()) {
				rs.close();
				ps.close();
				con.close();
				
				return null;
			}
			
			rs.next();
			text = rs.getString("extracted_text");
			
			rs.close();
			ps.close();
			con.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return text;
	}
	
	
	public static String getTessLangForId(int languageId) {
		String sql = "SELECT tess_lang FROM project_libri.ct_languages WHERE id = ?";
		
		String tessLang = null;
		
		try {
			Connection con = DBPCDataSource.getConnection();
			
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setInt(1, languageId);
			
			ResultSet rs = ps.executeQuery();
			
			if (!rs.isBeforeFirst()) {
				con.close();
				rs.close();
				ps.close();
				
				return null;
			}
			
			rs.next();
			
			tessLang = rs.getString("tess_lang");
			
			con.close();
			rs.close();
			ps.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return tessLang;
	}
	
	
	public static ArrayList<AuthorDO> getAllAuthors() {
		ArrayList<AuthorDO> authorsList = new ArrayList<AuthorDO>();
		
		String sql = "SELECT * FROM project_libri.ct_authors ORDER BY last_name;";
		
		try {
			Connection con = DBPCDataSource.getConnection();
			PreparedStatement ps = con.prepareStatement(sql);
			
			ResultSet rs = ps.executeQuery();
			
			while (rs.next()) {
				authorsList.add(new AuthorDO(rs.getInt("id"), rs.getString("first_name"), rs.getString("last_name")));
			}
			
			rs.close();
			ps.close();
			con.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
				
		return authorsList;
	}
	
	public static int addAuthor(String firstName, String lastName, String bio) {
		String sql = "INSERT INTO project_libri.ct_authors (first_name, last_name, bio) VALUES (?, ?, ?)";
		
		int insertId = -1; 
		
		try {
			Connection con = DBPCDataSource.getConnection();
			
			PreparedStatement ps = con.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
			ps.setString(1, firstName);
			ps.setString(2, lastName);
			if (bio == null || "".equals(bio)) {
				ps.setNull(3, Types.VARCHAR);
			} else {
				ps.setString(3, bio);
			}
			
			ps.executeUpdate();
			
			ResultSet rs = ps.getGeneratedKeys();
			
			if (rs.isBeforeFirst()) {
				rs.next();
				insertId = rs.getInt(1);
			}
			
			rs.close();
			ps.close();
			con.close();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return insertId;
	}
	
	
	public static int getDefaultAuthorId() {
		String sql = "SELECT min(id) FROM project_libri.ct_authors";
		int id = -1;
		
		try {
			Connection con = DBPCDataSource.getConnection();
			PreparedStatement ps = con.prepareStatement(sql);
			
			ResultSet rs = ps.executeQuery();
			
			if (rs.isBeforeFirst()) {
				rs.next();
				id = rs.getInt(1);
			}
			
			rs.close();
			ps.close();
			con.close();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
		return id;
	}
	
	
	public static boolean isTitleByAuthor(int titleId, int authorId) {
		String sql = "SELECT COUNT(*) FROM project_libri.ct_titles WHERE id = ? and author_id = ?";
		int rowsFound = -1;
		
		try {
			Connection con = DBPCDataSource.getConnection();
			PreparedStatement ps = con.prepareStatement(sql);
			
			ps.setInt(1, titleId);
			ps.setInt(2, authorId);
			
			ResultSet rs = ps.executeQuery();
			
			rs.next();
			rowsFound = rs.getInt(1);
			
			con.close();
			ps.close();
			rs.close();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return rowsFound > 0;
	}
	
	
	public static int getDefaultTitleIdForAuthor(int authorId) {
		String sql = "SELECT MIN(id) FROM project_libri.ct_titles WHERE author_id = ?";
		int id = -1;
		
		try {
			Connection con = DBPCDataSource.getConnection();
			PreparedStatement ps = con.prepareStatement(sql);
			
			ps.setInt(1, authorId);
			
			ResultSet rs = ps.executeQuery();
			
			rs.next();
			id = rs.getInt(1);
			
			con.close();
			ps.close();
			rs.close();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return id;
	}
	
	
	public static ArrayList<LanguageDO> getAllLanguages() {
		String sql = "SELECT * FROM project_libri.ct_languages";
		ArrayList<LanguageDO> languages = new ArrayList<LanguageDO>();
		
		try {
			Connection con = DBPCDataSource.getConnection();
			PreparedStatement ps = con.prepareStatement(sql);
			
			ResultSet rs = ps.executeQuery();
			
			while (rs.next()) {
				languages.add(new LanguageDO(rs.getInt("id"), rs.getString("language")));
			}
			
			rs.close();
			ps.close();
			con.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return languages;
	}
	
	
	public static BookDO getTitleForId(int id) {
		String sql = "SELECT * FROM project_libri.ct_titles WHERE id = ?";
		
		BookDO book = null;
		
		try {
			Connection con = DBPCDataSource.getConnection();
			PreparedStatement ps = con.prepareStatement(sql);
			
			ps.setInt(1, id);
			
			ResultSet rs = ps.executeQuery();
			
			if (rs.isBeforeFirst()) {
				rs.next();
				book = new BookDO(id, rs.getInt("author_id"), rs.getString("title"));
			}
			
			rs.close();
			ps.close();
			con.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return book;
	}
	
	
	public static String getLanguagForId(int id) {
		String sql = "SELECT language FROM project_libri.ct_languages WHERE id = ?";
		
		String language = null;
		
		try {
			Connection con = DBPCDataSource.getConnection();
			
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setInt(1, id);
			
			ResultSet rs = ps.executeQuery();
			
			if (!rs.isBeforeFirst()) {
				con.close();
				rs.close();
				ps.close();
				
				return null;
			}
			
			rs.next();
			
			language = rs.getString("language");
			
			con.close();
			rs.close();
			ps.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return language;
	}
	
	
	public static int addTitle(int authorId, String title, int defaultLanguageID, int year) {
		String sql = "INSERT INTO project_libri.ct_titles (title, author_id, default_language_id, `year`)"
				+ " VALUES (?, ?, ?, ?)";
		int insertId = -1;
		
		try {
			Connection con = DBPCDataSource.getConnection();
			PreparedStatement ps = con.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
			
			ps.setInt(2, authorId);
			ps.setString(1, title);
			ps.setInt(3, defaultLanguageID);
			ps.setInt(4, year);
			
			ps.executeUpdate();
			
			ResultSet rs = ps.getGeneratedKeys();
			
			if (rs.isBeforeFirst()) {
				rs.next();
				insertId = rs.getInt(1);
			}
			
			rs.close();
			ps.close();
			con.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return insertId;
	}
	
	
	public static int addBookEdition(BookEditionDO book) {
		String sql = "INSERT INTO project_libri.ct_books_editions (author_id, title_id, language_id, isbn, "
				+ " publication_year, publisher, language_title) "
				+ " VALUES (?, ?, ?, ?, ?, ?, ?)";
		
		int insertId = -1;
		
		try {
			Connection con = DBPCDataSource.getConnection();
			PreparedStatement ps = con.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
			
			ps.setInt(1, book.getAuthorId());
			ps.setInt(2, book.getTitleId());
			ps.setInt(3, book.getLanguageId());
			ps.setString(4, book.getIsbn());
			ps.setInt(5, book.getPublicationYear());
			ps.setString(6, book.getPublisher());
			ps.setString(7, book.getLanguageTitle());
			
			ps.executeUpdate();
			
			ResultSet rs = ps.getGeneratedKeys();
			if (rs.isBeforeFirst()) {
				rs.next();
				insertId = rs.getInt(1);
			}
			
			ps.close();
			con.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return insertId;
	}
	
	
	public static void setPdfForBook(int title_id, int language_id, String pdfName) {
		String sql = "UPDATE project_libri.ct_books_editions SET pdf_name = ? WHERE language_id = ? AND title_id = ?";
		
		try {
			Connection con = DBPCDataSource.getConnection();
			PreparedStatement ps = con.prepareStatement(sql);
			
			ps.setString(1, pdfName);
			ps.setInt(2, language_id);
			ps.setInt(3, title_id);
			
			ps.executeUpdate();
			
			ps.close();
			con.close();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	public static ArrayList<UserTypeDO> getUserTypes() {
		String sql = "SELECT * FROM project_libri.pl_users_type ";
		ArrayList<UserTypeDO> userTypes = new ArrayList<UserTypeDO>();
		
		try {
			Connection con = DBPCDataSource.getConnection();
			PreparedStatement ps = con.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			
			while (rs.next()) {
				userTypes.add(new UserTypeDO(rs.getInt("id"), rs.getString("type_name")));
			}
			
			
			con.close();
			ps.close();
			rs.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return userTypes;
	}
}

package com.projectlibri.utils;

import java.io.File;

import com.projectlibri.jdo.AuthorDO;
import com.projectlibri.jdo.BookDO;

public class FilesUtils {
	
	public static boolean isTempFile(File file) {
		if (file == null) {
			return false;
		}
		
		String filePath = file.getAbsolutePath();
		return filePath.startsWith(System.getProperty("temporary.transcriptions.absolute.directory"));
	}
	
	
	public static String generateFileName(int authorId, int titleId, String title, int languageId) {
		AuthorDO author = DBUtils.getAuthorForId(authorId);
		BookDO titleObj = DBUtils.getTitleForId(titleId);
		String language = DBUtils.getLanguagForId(languageId);
		
		title = title == null ? titleObj.getTitle() : title;
		
		String fileName = author.getFirstName().toLowerCase().replace(" ", "-") + "-" + author.getLastName().toLowerCase().replace(" ", "-") + "-" 
		+ title.toLowerCase().replace(" ", "-") + "-" + language.toLowerCase();
		
		return fileName;
	}
	
}

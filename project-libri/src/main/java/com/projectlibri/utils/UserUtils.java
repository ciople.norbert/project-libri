package com.projectlibri.utils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.projectlibri.db.DBPCDataSource;
import com.projectlibri.jdo.UserDO;
import com.projectlibri.login.ValidationService;

public class UserUtils {
	
	public static UserDO getUserByCredentials(String identifier, String password) {
		String sql = "SELECT id, username, email, user_type_id FROM project_libri.pl_users "
				+ "WHERE (username = ? or email = ?) and password = ?";
		
		try {
			Connection con = DBPCDataSource.getConnection();

			PreparedStatement ps = con.prepareStatement(sql);
			ps.setString(1, identifier);
			ps.setString(2, identifier);
			ps.setString(3, new ValidationService().generatePasswordHash(password));
			
			ResultSet rs = ps.executeQuery();
			
			if (!rs.isBeforeFirst()) {
				throw new RuntimeException("Cannot fetch data for user: " + identifier + " using credentials");
			}
			
			rs.next();
			
			UserDO userData = new UserDO(rs.getInt("id"), rs.getString("username"), rs.getString("email"), rs.getInt("user_type_id"));
			
			rs.close();
			ps.close();
			
			return userData;
			
		} catch (SQLException e) {
			e.printStackTrace();
		} 
		
		return null;
	}
	
	
	public static boolean userTypeHasAccessToView(int userType, String view) {
		int access = 0;
		
		String sql = "SELECT type_access FROM project_libri.pl_views_access WHERE `name` LIKE ? AND user_type = ?;";
		
		try {
			Connection con = DBPCDataSource.getConnection();
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setString(1, view);
			ps.setInt(2, userType);
			
			ResultSet rs = ps.executeQuery();
			
			if (!rs.isBeforeFirst()) {
				con.close();
				ps.close();
				rs.close();
			
				return false;
			}
			
			rs.next();
			access = rs.getInt("type_access");
			rs.next();
			boolean isResultUnique = rs.isAfterLast();
			
			rs.close();
			ps.close();
			con.close();
			
			if (!isResultUnique) {
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return access == 1;
	}

}

package com.projectlibri.utils;

import java.util.ArrayList;

import com.projectlibri.jdo.AuthorDO;
import com.projectlibri.jdo.BookDO;

public class DataUtils {

	public static ArrayList<AuthorDO> getDistinctAuthors(ArrayList<BookDO> booksList) {
		ArrayList<AuthorDO> distinctAuthorsList =  new ArrayList<AuthorDO>();
		
		for (BookDO book: booksList) {
			AuthorDO author = new AuthorDO(book.getAuthorId(), book.getAuthorFirstName(), book.getAuthorLastName());
			
			if (!distinctAuthorsList.contains(author)) {
				distinctAuthorsList.add(author);
			}
		}
		
		return distinctAuthorsList;
	}
	
	
	public static ArrayList<BookDO> getDistinctTitles(ArrayList<BookDO> booksList) {
		ArrayList<BookDO> distinctTitlesList =  new ArrayList<BookDO>();
		
		for (BookDO book: booksList) {

			if (!distinctTitlesList.contains(book) && book.getId() != -1) {
				distinctTitlesList.add(book);
			}
		}
		
		return distinctTitlesList;
	}
	
	
	public static boolean isStringNumeric(String input) {
		if (input == null || input.isBlank() || input.isEmpty()) {
			return false;
		}
		
		for (int i = 0; i < input.length(); i++) {
			if (!('0' <= input.charAt(i) && input.charAt(i) <= '9')) {
				return false;
			}
		}
		
		return true;
	}
	
}

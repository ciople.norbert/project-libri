<%@page import="com.projectlibri.jdo.UserDO"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<%
UserDO userData = (UserDO) session.getAttribute("userData");

String username = "";
if (userData == null) {
	response.sendRedirect("/project-libri/login");
	return;
} else {
	username = userData.getUsername();	
}

%>
<head>
<meta charset="ISO-8859-1">
<title>Please wait</title>
</head>
<body>
<p>Extracting text from file, this may take a while...</p>
<form name="auto" action="transcribeAction">
    <input type="hidden" name="userId" value="<%=request.getParameter("userId")%>">
    <input type="hidden" name="bookId" value="<%=request.getParameter("bookId")%>">
    <input type="hidden" name="languageId" value="<%=request.getParameter("languageId")%>">
    <input type="hidden" name="pdfFile" value="<%=request.getParameter("pdfFile")%>">
    <input type="hidden" name="action" value="<%=request.getParameter("action")%>">
</form>
<script>
    document.auto.submit();
</script>
</body>
</html>
<%@page import="com.projectlibri.jdo.BookDO"%>
<%@page import="com.projectlibri.utils.DataUtils"%>
<%@page import="com.projectlibri.utils.DBUtils"%>
<%@page import="com.projectlibri.jdo.AuthorDO"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.projectlibri.utils.UserUtils"%>
<%@page import="com.projectlibri.jdo.UserDO"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
    
<!DOCTYPE html>
<html>
<%
UserDO userData = (UserDO) session.getAttribute("userData");

String username = "";
if (userData == null) {
	response.sendRedirect("/project-libri/login");
	return;
} else {
	username = userData.getUsername();	
}

int authorId;
if (request.getParameter("authorId") == null 
	|| !DataUtils.isStringNumeric(request.getParameter("authorId")) 
	|| !DBUtils.authorWithIdExists(Integer.parseInt(request.getParameter("authorId")))) {
	authorId = DBUtils.getDefaultAuthorId();
} else {
	authorId = Integer.parseInt(request.getParameter("authorId")); 
}


int titleId;
if (request.getParameter("titleId") == null 
	|| !DataUtils.isStringNumeric(request.getParameter("titleId")) 
	|| !DBUtils.isTitleByAuthor(Integer.parseInt(request.getParameter("titleId")), authorId)) {
	titleId = DBUtils.getDefaultTitleIdForAuthor(authorId);
} else {
	titleId = Integer.parseInt(request.getParameter("titleId")); 
}

ArrayList<AuthorDO> authorsList = DBUtils.getAllAuthors();
ArrayList<BookDO> titlesList = DBUtils.getBooksByAuthor(authorId);

pageContext.setAttribute("showAdministrative", 
		UserUtils.userTypeHasAccessToView(userData.getUserTypeId(), "administrative") ? 1 : 0);
pageContext.setAttribute("authorsList", authorsList);
pageContext.setAttribute("titlesList", titlesList);
pageContext.setAttribute("numTitles", titlesList.size());
pageContext.setAttribute("authorId", authorId);
pageContext.setAttribute("titleId", titleId);
pageContext.setAttribute("languages", DBUtils.getAllLanguages());


%>
<head>
<meta charset="ISO-8859-1">
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
<link rel="stylesheet" href="${pageContext.request.contextPath}/styles/plStyles.css">
<title>Project Libri | Add a book</title>
</head>
<body>
 		
	<!-- Navbar -->
	<nav class="navbar navbar-expand-lg navbar-light bg-light bottom-bordered">
	<div class="container-fluid">
		<img class="navbar-class" src="../img/home.png" alt="home" height="40px" width="40px" 
  	onclick="location.href ='${pageContext.request.contextPath}/views/index.jsp'">
			<div class="collapse navbar-collapse" id="navbarSupportedContent">
				<ul class="navbar-nav me-auto mb-2 mb-lg-0">
					<li class="nav-item dropdown">
						<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
            My Account
          </a>
						<ul class="dropdown-menu" aria-labelledby="navbarDropdown">
							<li>
								<a class="dropdown-item" href ='${pageContext.request.contextPath}/accountAction?action=details'>Account details</a>
							</li>
							<li>
								<a class="dropdown-item" href ='${pageContext.request.contextPath}/accountAction?action=logout'>Sign out</a>
							</li>
						</ul>
					</li>
				</ul>
				<form class="d-flex" action="../search">
					<input class="form-control me-2" type="search" placeholder="Search for author, title or ISBN" aria-label="Search" size=100 name="searchInput">
						<button class="btn btn-outline-success" type="submit">Search</button>
					</form>
				</div>
			</div>
		</nav>
		<!-- Navbar -->
		
		<!-- Sidebar -->
        <div id="sidebar-wrapper" class="bg-light right-bordered">
            <ul class="sidebar-nav vertical-centered">
                <li>
                    <a href="browse.jsp">Browse</a>
                </li>
                <li>
                    <a href="myBooks.jsp">My books</a>
                </li>
                <li>
                    <a href="about.jsp">About</a>
                </li>
                <li>
                     <c:if test="${showAdministrative == 1}">
	                <li>
	                    <a href="administrative.jsp">Administrative</a>
	                </li>
                </c:if>
                </li>
                <!--  <li>
                    <a href="request.jsp">Request</a>
                </li>  -->
            </ul>
        </div>
        <!-- /Sidebar -->
        
        <div id="page-content-wrapper">
        	<div class="container-fluid">
        		<div class="row">
                    <div class="col-lg-12">
						
						<form action="/project-libri/bookAction" method="POST" enctype="multipart/form-data">
							<div class="form-input-container">
								<label for="author-id-input">Author ID:</label>
								<input type="text" name="authorId" list="authorsList" id="author-id-input" value="${authorId}" 
								onchange="switchAuthor();" class="form-control">
								<datalist id="authorsList">
									<c:forEach items="${authorsList}" var="author">
										<%--  <option value="${author}; id= ${author.id}"></option>--%>
										<option value="${author.id}"><c:out value="${author}"></c:out></option>
									</c:forEach>								
								</datalist>
								<p style="color:gray"><i>Author not in list? Click <a href="addAuthor.jsp">here</a> to add a new author.</i></p>
							</div>
							
							<div class="form-input-container">
								<label for="author-id-input">Existing titles for author:</label>
								<input class="form-control" type="text" name="titleId" list="titlesList" id="title-id-input" value="${titleId}" 
								onchange="switchTitle();" ${numTitles == 0 ? "disabled" : '' }>
								<datalist id="titlesList">
									<c:if test="${numTitles != 0}">
										<c:forEach items="${titlesList}" var="title">
											<%--  <option value="${author}; id= ${author.id}"></option>--%>
											<option value="${title.id}"><c:out value="${title.title}"></c:out></option>
										</c:forEach>	
									</c:if>
								</datalist>
							</div>
							
							<div class="form-input-container">
								<i><label>Use an existing title</label></i>
								<input type="checkbox" name="existingTitle" id="use-existing-title-check" value="true" ${numTitles != 0 ? "checked" : '' }
								${numTitles == 0 ? "disabled" : '' } onclick="toggleTitleInputs()">
							</div>
						
							<div class="form-input-container">	
								<label>New title</label>
								<input class="form-control" type="text" name="titleInput" id="title-input" ${numTitles != 0 ? "disabled" : '' } onkeyup="setIsSubmitButtonDisabled();">
							</div>
							
							<div class="form-input-container">
								<label>Original publication year</label>
								<input class="form-control" type="number" name="originalPublicationYearInput" id="original-publication-year-input" 
								${numTitles != 0 ? "disabled" : '' } onkeyup="setIsSubmitButtonDisabled();">
							</div>
							
							<div class="form-input-container">
								<label>Language title</label>
								<input class="form-control" type="text" name="languageTitleInput" id="language-title-input" disabled onchange="setIsSubmitButtonDisabled();">
							</div>
							
							
							<div class="form-input-container">
								<i><label>Use same title</label></i>
								<input type="checkbox" name="useSameTitle" value="true" id="use-same-title-check" checked onclick="toggleSameTitle()">
							</div>
							
							<div class="form-input-container">
								<label>Select language</label>
								<select id="language-select" name="languageSelect">
									<c:forEach items="${languages}" var="language" >
										<option value="${language.id}">${language.language}</option>
									</c:forEach>
								</select>
							</div>

							<div class="form-input-container">
								<label>ISBN</label>
								<input class="form-control" type="text" name="isbnInput" id="isbn-input" onkeyup="setIsSubmitButtonDisabled();">
							</div>

							<div class="form-input-container">
								<label>Publisher</label>
								<input class="form-control" type="text" name="publisherInput" id="publisher-input" onkeyup="setIsSubmitButtonDisabled();">
							</div>
							
							<div class="form-input-container">
								<label>Publication year</label>
								<input class="form-control" type="number" name="publicationYearInput" id="publication-year-input" onkeyup="setIsSubmitButtonDisabled();">
							</div>
							
							
							Choose a file: <input type="file" name="fileUpload" accept=".pdf"/>
							
							<br>
							<label>Add to index </label>
							<input  type="checkbox" name="updateIndex" value="update" checked>
							
							<br>
							<br>
							<input type="submit" value="Submit" id="submit-button">
							
							
							<input type="hidden" name="action" value="add">
						</form>
						
					</div>
				</div>
			</div>
		</div>
		
		
		<script>
			setIsSubmitButtonDisabled();
		
			function switchAuthor() {
				authorId = document.getElementById("author-id-input").value;
				window.location.href = "addBook.jsp?authorId=" + authorId;
				setIsSubmitButtonDisabled();
			}
			
			
			function toggleTitleInputs() {
				//window.alert("toggle");
				document.getElementById("title-id-input").disabled = !document.getElementById("title-id-input").disabled;
				document.getElementById("title-input").disabled = !document.getElementById("title-input").disabled;
				document.getElementById("original-publication-year-input").disabled = !document.getElementById("original-publication-year-input").disabled;
				setIsSubmitButtonDisabled();
			}
			
			
			function toggleSameTitle() {
				document.getElementById("language-title-input").disabled = !document.getElementById("language-title-input").disabled;
				setIsSubmitButtonDisabled();
			}
			
			
			function switchTitle() {
				authorId = document.getElementById("author-id-input").value;
				bookId = document.getElementById("title-id-input").value;
				window.location.href = "addBook.jsp?authorId=" + authorId + "&titleId=" + bookId; 
			}
			

			function setIsSubmitButtonDisabled() {
				var useExistingTitle = document.getElementById("use-existing-title-check").checked;
				var submitButton = document.getElementById("submit-button");
				
				if (!useExistingTitle) {
					var newTitle = document.getElementById('title-input').value;
					if (newTitle == null || newTitle == "") {
						submitButton.disabled = true;
						return;
					}
					
					var originalPubYear = document.getElementById('original-publication-year-input').value;
					if (originalPubYear == null || originalPubYear == "") {
						submitButton.disabled = true;
						return;
					}
				}
				
				var useSameTitle = document.getElementById("use-same-title-check").checked;
				var languageTitle = document.getElementById('language-title-input').value; 
				
				if (!useSameTitle && (languageTitle == null || languageTitle == "")) {
					submitButton.disabled = true;
					return;
				}
				
				var publisher = document.getElementById("publisher-input").value;
				
				if (publisher == null || publisher == "") {
					submitButton.disabled = true;
					return;
				}
				
				
				var publicationYear = document.getElementById("publication-year-input").value;
				
				if (publicationYear == null || publicationYear == "") {
					submitButton.disabled = true;
					return;
				}
				
				
				
				var isbn = document.getElementById("isbn-input").value;
				
				if (isbn == null || isbn == "") {
					submitButton.disabled = true;
					return;
				}
				
				submitButton.disabled = false;
			} 
		</script>

	<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
</body>
</html>
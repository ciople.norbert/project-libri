<%@page import="com.projectlibri.utils.UserUtils"%>
<%@page import="com.projectlibri.jdo.BookDO"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.projectlibri.jdo.AuthorDO"%>
<%@page import="com.projectlibri.utils.DBUtils"%>
<%@page import="com.projectlibri.utils.DataUtils"%>
<%@page import="com.projectlibri.jdo.UserDO"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
    
<!DOCTYPE html>
<html>
<%
UserDO userData = (UserDO) session.getAttribute("userData");

String username = "";
if (userData == null) {
	response.sendRedirect("/project-libri/login");
	return;
} else {
	username = userData.getUsername();	
}

String idParam = request.getParameter("authorId");
if (!DataUtils.isStringNumeric(idParam)) {
	response.sendRedirect("index.jsp");
	return;
}

int authorId = Integer.parseInt(idParam);

if (!DBUtils.authorWithIdExists(authorId)) {
	response.sendRedirect("index.jsp");
	return;
}

System.out.println("author id: " + authorId);

AuthorDO author = DBUtils.getAuthorForId(authorId);
ArrayList<BookDO> authorBooks = DBUtils.getBooksByAuthor(author);

pageContext.setAttribute("booksResult", authorBooks);
pageContext.setAttribute("numResults", authorBooks.size());
pageContext.setAttribute("authorBio", author.getBio());
pageContext.setAttribute("showAdministrative", 
		UserUtils.userTypeHasAccessToView(userData.getUserTypeId(), "administrative") ? 1 : 0);
%>
<head>
<meta charset="ISO-8859-1">
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
<link rel="stylesheet" href="${pageContext.request.contextPath}/styles/plStyles.css">
<title>Project Libri | Request</title>
</head>
<body>
 		
	<!-- Navbar -->
	<nav class="navbar navbar-expand-lg navbar-light bg-light bottom-bordered">
	<div class="container-fluid">
		<img class="navbar-class" src="../img/home.png" alt="home" height="40px" width="40px" 
  	onclick="location.href ='${pageContext.request.contextPath}/views/index.jsp'">
			<div class="collapse navbar-collapse" id="navbarSupportedContent">
				<ul class="navbar-nav me-auto mb-2 mb-lg-0">
					<li class="nav-item dropdown">
						<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
            My Account
          </a>
						<ul class="dropdown-menu" aria-labelledby="navbarDropdown">
							<li>
								<a class="dropdown-item" href ='${pageContext.request.contextPath}/accountAction?action=details'>Account details</a>
							</li>
							<li>
								<a class="dropdown-item" href ='${pageContext.request.contextPath}/accountAction?action=logout'>Sign out</a>
							</li>
						</ul>
					</li>
				</ul>
				<form class="d-flex" action="../search">
					<input class="form-control me-2" type="search" placeholder="Search for author, title or ISBN" aria-label="Search" size=100 name="searchInput">
						<button class="btn btn-outline-success" type="submit">Search</button>
					</form>
				</div>
			</div>
		</nav>
		<!-- Navbar -->
		
		<!-- Sidebar -->
        <div id="sidebar-wrapper" class="bg-light right-bordered">
            <ul class="sidebar-nav vertical-centered">
                <li>
                    <a href="browse.jsp">Browse</a>
                </li>
                <li>
                    <a href="myBooks.jsp">My books</a>
                </li>
                <li>
                    <a href="about.jsp">About</a>
                </li>
                <li>
                     <c:if test="${showAdministrative == 1}">
	                <li>
	                    <a href="administrative.jsp">Administrative</a>
	                </li>
                </c:if>
                </li>
                <!--  <li>
                    <a href="request.jsp">Request</a>
                </li>  -->
            </ul>
        </div>
        <!-- /Sidebar -->
        
        <div id="page-content-wrapper">
        	<div class="container-fluid">
        		<div class="row">
                    <div class="col-lg-12">
						<h2><% out.println(author.getFirstName()); %>
						<% out.println(author.getLastName()); %></h2>
						
						<p class="author-bio">
							<c:if test="${authorBio == null}">This author doesn't have a bio yet</c:if>
							<c:if test="${authorBio != null}"><c:out value="${authorBio}"></c:out> </c:if>
						</p>
						
						<h3>Books by this author</h3>
						<c:if test="${numResults == 0}">
							<li>We currently have no books by this author</li>
						</c:if>
						
						<c:forEach items="${booksResult}" var="result">
							<li><a href="bookDetails.jsp?bookId=${result.id}">${result.title}</a></li>
						</c:forEach>
						
					</div>
				</div>
			</div>
		</div>
	
	<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>

</body>
</html>
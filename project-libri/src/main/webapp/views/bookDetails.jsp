<%@page import="java.util.HashMap"%>
<%@page import="com.projectlibri.jdo.BookEditionDO"%>
<%@page import="com.projectlibri.utils.DBUtils"%>
<%@page import="com.projectlibri.utils.DataUtils"%>
<%@page import="com.projectlibri.utils.UserUtils"%>
<%@page import="com.projectlibri.jdo.UserDO"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<!DOCTYPE html>
<html>
<%
UserDO userData = (UserDO) session.getAttribute("userData");

String username = "";
if (userData == null) {
	response.sendRedirect("/project-libri/login");
	return;
} else {
	username = userData.getUsername();	
}

String idParam = request.getParameter("bookId");
System.out.println("idParam: " + idParam);
if (!DataUtils.isStringNumeric(idParam)) {
	response.sendRedirect("index.jsp");
	return;
}

int bookId = Integer.parseInt(idParam);

if (!DBUtils.titleWithIdExists(bookId)) {
	response.sendRedirect("index.jsp");
	return;
}

System.out.println("book id: " + bookId);

int defaultLanguageId = DBUtils.getDefaultLanguageId(bookId);
System.out.println("default language id: " + defaultLanguageId);
int languageId = defaultLanguageId;

idParam = request.getParameter("languageId");
if (DataUtils.isStringNumeric(idParam)) {
	languageId = Integer.parseInt(idParam);
}

System.out.println("language id: " + languageId);

BookEditionDO bookEdition = DBUtils.getBookEdition(bookId, languageId);

if (bookEdition == null) {
	bookEdition = DBUtils.getBookEdition(bookId, defaultLanguageId);
	languageId = defaultLanguageId;
}

int userId = userData.getId();
int isBookInCollection = DBUtils.isBookInUserCollection(bookId, languageId, userId) ? 1 : 0;

HashMap<Integer, String> bookLanguages = DBUtils.getLanguagesForTitle(bookId);
pageContext.setAttribute("bookLanguages", bookLanguages);
pageContext.setAttribute("selectedLanguageId", languageId);
pageContext.setAttribute("authorId", bookEdition.getAuthorId());
pageContext.setAttribute("userId", userId);
pageContext.setAttribute("bookId", bookId);
pageContext.setAttribute("pdfFile", bookEdition.getPdfLocation());
pageContext.setAttribute("pdfFileAbsolute", bookEdition.getPdfAbsoluteLocation());
pageContext.setAttribute("isBookInUserColection", isBookInCollection);
pageContext.setAttribute("showTranscribeButton", 
		UserUtils.userTypeHasAccessToView(userData.getUserTypeId(), "transcribe") ? 1 : 0);
pageContext.setAttribute("showUploadButton", 
		UserUtils.userTypeHasAccessToView(userData.getUserTypeId(), "fileUpload") ? 1 : 0);
pageContext.setAttribute("isBookTranscribed", DBUtils.bookHasExtractedText(bookId, languageId) ? 1 : 0);
pageContext.setAttribute("showAdministrative", 
		UserUtils.userTypeHasAccessToView(userData.getUserTypeId(), "administrative") ? 1 : 0);

System.out.println("IS BOOK IN COLLECTION: " + DBUtils.isBookInUserCollection(bookId, languageId, userId));
%>
<head>
<meta charset="ISO-8859-1">
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
<link rel="stylesheet" href="${pageContext.request.contextPath}/styles/plStyles.css">
<title>Project Libri | Book details</title>
</head>
<body>
 		
	<!-- Navbar -->
	<nav class="navbar navbar-expand-lg navbar-light bg-light bottom-bordered">
	<div class="container-fluid">
		<img class="navbar-class" src="../img/home.png" alt="home" height="40px" width="40px" 
  	onclick="location.href ='${pageContext.request.contextPath}/views/index.jsp'">
			<div class="collapse navbar-collapse" id="navbarSupportedContent">
				<ul class="navbar-nav me-auto mb-2 mb-lg-0">
					<li class="nav-item dropdown">
						<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
            My Account
          </a>
						<ul class="dropdown-menu" aria-labelledby="navbarDropdown">
							<li>
								<a class="dropdown-item" href ='${pageContext.request.contextPath}/accountAction?action=details'>Account details</a>
							</li>
							<li>
								<a class="dropdown-item" href ='${pageContext.request.contextPath}/accountAction?action=logout'>Sign out</a>
							</li>
						</ul>
					</li>
				</ul>
				<form class="d-flex" action="../search">
					<input class="form-control me-2" type="search" placeholder="Search for author, title or ISBN" aria-label="Search" size=100 name="searchInput">
						<button class="btn btn-outline-success" type="submit">Search</button>
					</form>
				</div>
			</div>
		</nav>
		<!-- Navbar -->
		
		<!-- Sidebar -->
        <div id="sidebar-wrapper" class="bg-light right-bordered">
            <ul class="sidebar-nav vertical-centered">
                <li>
                    <a href="browse.jsp">Browse</a>
                </li>
                <li>
                    <a href="myBooks.jsp">My books</a>
                </li>
                <li>
                    <a href="about.jsp">About</a>
                </li>
                <li>
                    <c:if test="${showAdministrative == 1}">
	                <li>
	                    <a href="administrative.jsp">Administrative</a>
	                </li>
                </c:if>
                </li>
               <!--  <li>
                    <a href="request.jsp">Request</a>
                </li>  -->
            </ul>
        </div>
        <!-- /Sidebar -->
        
        <div id="page-content-wrapper">
        	<div class="container-fluid">
        		<div class="row">
                    <div class="col-lg-12">
						<h3><a class="black-anchor" href="authorDetails.jsp?authorId=${authorId}">
						<% out.println(bookEdition.getAuthorFirstName()); %>
						<% out.println(bookEdition.getAuthorLastName()); %> </a></h3>
						<h2><% out.println(bookEdition.getLanguageTitle());%>
						<% out.println("(" + bookEdition.getYear() + ")"); %></h2>
						<p><% out.println(bookEdition.getPublisher() + ","); %>
						<% out.println(bookEdition.getPublicationYear()); %></p>
						
					</div>
					 <div>
						<select id="language-select" onchange="switchLanguage()" class="book-details-btn switch-languge-select">
							<c:forEach items="${bookLanguages}" var="language">
								<option value="${language.key}" ${language.key == selectedLanguageId ? 'selected="selected"' : '' }>
								${language.value}</option>
							</c:forEach>
						</select>
					
					
					
					 
					 	<form action="../collectionAction" method="POST" class="book-details-form">
					 		<input type="hidden" name="userId" value="${userId}">
					 		<input type="hidden" name="bookId" value="${bookId}">
					 		<input type="hidden" name="languageId" value="${selectedLanguageId}">
					 		
					 		<!-- <c:out value="BOOK IN COLLECTION: ${isBookInUserColection}"></c:out> -->
					 		
					 		<c:if test="${isBookInUserColection == 0}">
					 			<button id="collection-add-btn" type="submit" name="action" value="add"
					 			class="book-details-btn btn-green">Add to my collection</button>
					 		</c:if>
					 		
					 		<c:if test="${isBookInUserColection == 1}">
					 			<button id="collection-add-btn" type="submit" name="action" value="remove"
					 			class="book-details-btn btn-red">Remove from my collection</button>
					 		</c:if>
					 	</form>
					 
					 
					 
					  <c:if test="${showTranscribeButton == 1}">
						 
						 	<form action="../transcribe" target="_blank" class="book-details-form">
						 		<input type="hidden" name="userId" value="${userId}">
						 		<input type="hidden" name="bookId" value="${bookId}">
						 		<input type="hidden" name="languageId" value="${selectedLanguageId}">
						 		<input type="hidden" name="pdfFile" value="${pdfFileAbsolute}">
						 		
						 		<button id="transcribe-btn" type="submit" name="action" value = "transcribe" class="book-details-btn"
						 		${pdfFile == null ? 'disabled' : ''}>Transcribe this book</button>
						 	</form>
						 
					 </c:if>
					 
					 
					 	<form action="../views/reader.jsp" target="_blank" class="book-details-form">
					 		<input type="hidden" name="bookId" value="${bookId}">
					 		<input type="hidden" name="languageId" value="${selectedLanguageId}">
					 		
						 	<button id="reader-btn" type="submit" name="action" value = "read" class="book-details-btn"
						 	${isBookTranscribed == 0 ? 'disabled' : ''}>Open transcribed version</button>
						</form>
					
					</div>
					 
					 <c:if test="${pdfFile != null}">
						 <iframe class="pdf-container" src="${pdfFile}#toolbar=0">
	    				</iframe>
    				</c:if>
    				
    				<br>
    				<c:if test="${pdfFile == null && showUploadButton == 1}">
						 <form action="/project-libri/bookAction" method="POST" enctype="multipart/form-data">
						 
						 <input type="submit" value="Add PDF file" id="pdf-submit">
						 
						 Choose a file: <input type="file" name="fileUpload" 
						 accept=".pdf" id="file-upload" onchange="setIsFileUploadButtonDisabled();"/>
						 
						 
						 <input type="hidden" name="authorId" value="${authorId}">
						 <input type="hidden" name="titleId" value="${bookId}">
						 <input type="hidden" name="languageId" value="${selectedLanguageId}">
						 <input type="hidden" name="action" value="uploadPdf">
						 </form>
						 
						 
    				</c:if>
					 </div>
				
			</div>
		</div>
		
	
	<script>
		setIsFileUploadButtonDisabled();
	
		function switchLanguage() {
			languageId = document.getElementById("language-select").value;
			const urlString = window.location.search;
			const urlParams = new URLSearchParams(urlString);
			const bookId = urlParams.get("bookId");
			window.location.href = "bookDetails.jsp?bookId=" + bookId + "&languageId=" + languageId; 
		}
		
		
		function setIsFileUploadButtonDisabled() {
			var file = document.getElementById("file-upload");
			var submitButton = document.getElementById("pdf-submit");
			
			submitButton.disabled = file.files.length == 0;
		}
	</script>
	
	<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
	
</body>
</html>
<%@page import="com.projectlibri.utils.FilesUtils"%>
<%@page import="org.apache.commons.io.FileUtils"%>
<%@page import="java.io.File"%>
<%@page import="com.projectlibri.jdo.UserDO"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<%
UserDO userData = (UserDO) session.getAttribute("userData");

String username = "";
if (userData == null) {
	response.sendRedirect("/project-libri/login");
	return;
} else {
	username = userData.getUsername();	
}

String fileName = request.getParameter("file");
File file = new File(System.getProperty("temporary.transcriptions.absolute.directory") + fileName + ".txt");

if (file == null) {
	file = new File(System.getProperty("temporary.transcriptions.absolute.directory") + fileName + ".txt");
}

String fileText = FileUtils.readFileToString(file);

if (FilesUtils.isTempFile(file)) {
	file.delete();
}

%>
<meta charset="ISO-8859-1">
<title>Editor</title>
</head>
<body>
<textarea rows="50" cols="200"><%out.println(fileText);%></textarea>
<form action=<%System.out.println("ACTINO"); %>>
	<button onclick=<% %>>ACTION</button>
</form>
</body>
</html>
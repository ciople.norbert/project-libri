<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>    
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Project Libri | Register</title>
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
<link rel="stylesheet" href="${pageContext.request.contextPath}/styles/plStyles.css">

</head>
<body onload="validateForm()">
	
	<div class="register-container" name=register-container>
	<form action="register" method="POST">
		 <div>	
		 <div class="form-group">
		    <label for="mail">E-Mail address</label>
		    <input type="text" class="form-control" id="mail" name="mailAddress" onkeyup="validateForm()">
  		</div>
  		
  		<div class="form-group">
		    <label for="username">Username</label>
		    <input type="text" class="form-control" id="user" name="username" onkeyup="validateForm()">
  		</div>
  		
  		<div class="form-group">
		    <label for="pwd">Password:</label>
		    <input type="password" class="form-control" id="pwd" name="password" onkeyup="validateForm()">
  		</div>
  		
  		<div class="form-group">
		    <label for="pwd2">Repeat password:</label>
		    <input type="password" class="form-control" id="pwd2" name="password2" onkeyup="validateForm()">
  		</div>
  		
  		<div class="form-group">
		    <label for="first-name">First name:</label>
		    <input type="text" class="form-control" id="first-name" name="firstName" onkeyup="validateForm()">
  		</div>
  		
  		<div class="form-group">
		    <label for="last-name">Last name:</label>
		    <input type="text" class="form-control" id="last-name" name="lastName" onkeyup="validateForm()">
  		</div>
  		
  		</div>
		
		<div class="form-group" id="login-button-container">
			<button type="submit" class="btn btn-default auth-action-btn" id="login-button">Register</button>
		</div>
		
	</form>
	</div>
	
	<div id="error-message-container">
		<c:if test="${requestScope.errorMessage != null}">
			<p>${requestScope.errorMessage}</p>
		</c:if>
	</div>
	
	<script>		
			function validateForm() {
				
				var submitButton = document.getElementById("login-button");
				
				var fieldIds = ["mail", "user", "pwd", "pwd2", "first-name", "last-name"];
				
				for (var i = 0; i < fieldIds.length; i++) {
					if (document.getElementById(fieldIds[i]).value == "" || document.getElementById(fieldIds[i]).value == null) {
						submitButton.disabled = true;
						return;
					}
				}
				
				submitButton.disabled = false;
			}
		</script>
	
	<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
	
</body>
</html>
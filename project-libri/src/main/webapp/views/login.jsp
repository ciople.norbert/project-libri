<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Project Libri | Login</title>
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
<link rel="stylesheet" href="${pageContext.request.contextPath}/styles/plStyles.css">
</head>
<body>
	
	<div class="login-container" name=login-container>
	<form action="login" method="POST">
		 <div>	
		 <div class="form-group">
		    <label for="username">Username or E-mail address:</label>
		    <input type="text" class="form-control" id="user" name="username">
  		</div>
  		
  		<div class="form-group">
		    <label for="pwd">Password:</label>
		    <input type="password" class="form-control" id="pwd" name="password">
  		</div>
  		</div>
		
		<div class="form-group" id="login-button-container">
			<button type="submit" class="btn btn-default auth-action-btn" id="login-button">Login</button>
			
		</div>
		
		<div class="recovery-link-container">
			<a class="recovery-link" href="recover.jsp">Forgot my password</a>
		</div>
		
	</form>
	
	
	
	<form action="register">
		<div class="form-group" id="register-button-container">
			<button type="submit" class="btn btn-default auth-action-btn" id="register-button">Create account</button>
		</div>
	</form>
	</div>
	
	<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
	
</body>
</html>
<%@page import="com.projectlibri.utils.DBUtils"%>
<%@page import="com.projectlibri.utils.UserUtils"%>
<%@page import="com.projectlibri.jdo.UserDO"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
    
<!DOCTYPE html>
<html>
<%
UserDO userData = (UserDO) session.getAttribute("userData");

String username = "";
if (userData == null) {
	response.sendRedirect("/project-libri/login");
	return;
} else {
	username = userData.getUsername();	
}

pageContext.setAttribute("showAdministrative", 
		UserUtils.userTypeHasAccessToView(userData.getUserTypeId(), "administrative") ? 1 : 0);
pageContext.setAttribute("userTypes", DBUtils.getUserTypes());

%>
<head>
<meta charset="ISO-8859-1">
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
<link rel="stylesheet" href="${pageContext.request.contextPath}/styles/plStyles.css">
<title>Project Libri | Create user</title>
</head>
<body onload="validateForm()">
 		
	<!-- Navbar -->
	<nav class="navbar navbar-expand-lg navbar-light bg-light bottom-bordered">
	<div class="container-fluid">
		<img class="navbar-class" src="../img/home.png" alt="home" height="40px" width="40px" 
  	onclick="location.href ='${pageContext.request.contextPath}/views/index.jsp'">
			<div class="collapse navbar-collapse" id="navbarSupportedContent">
				<ul class="navbar-nav me-auto mb-2 mb-lg-0">
					<li class="nav-item dropdown">
						<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
            My Account
          </a>
						<ul class="dropdown-menu" aria-labelledby="navbarDropdown">
							<li>
								<a class="dropdown-item" href ='${pageContext.request.contextPath}/accountAction?action=details'>Account details</a>
							</li>
							<li>
								<a class="dropdown-item" href ='${pageContext.request.contextPath}/accountAction?action=logout'>Sign out</a>
							</li>
						</ul>
					</li>
				</ul>
				<form class="d-flex" action="../search">
					<input class="form-control me-2" type="search" placeholder="Search for author, title or ISBN" aria-label="Search" size=100 name="searchInput">
						<button class="btn btn-outline-success" type="submit">Search</button>
					</form>
				</div>
			</div>
		</nav>
		<!-- Navbar -->
		
		<!-- Sidebar -->
        <div id="sidebar-wrapper" class="bg-light right-bordered">
            <ul class="sidebar-nav vertical-centered">
                <li>
                    <a href="browse.jsp">Browse</a>
                </li>
                <li>
                    <a href="myBooks.jsp">My books</a>
                </li>
                <li>
                    <a href="about.jsp">About</a>
                </li>
                <li>
                     <c:if test="${showAdministrative == 1}">
	                <li>
	                    <a href="administrative.jsp">Administrative</a>
	                </li>
                </c:if>
                </li>
                <!--  <li>
                    <a href="request.jsp">Request</a>
                </li>  -->
            </ul>
        </div>
        <!-- /Sidebar -->
        
        <div id="page-content-wrapper">
        	<div class="container-fluid">
        		<div class="row">
                    <div class="col-lg-12">
						
						<div>
						<form action="../register" method="POST">
							 <div>	
							 <div class="form-input-container">
							    <label for="mail">E-Mail address</label>
							    <input type="text" class="form-control" id="mail" name="mailAddress" onkeyup="validateForm()">
					  		</div>
					  		
					  		<div class="form-input-container">
							    <label for="username">Username</label>
							    <input type="text" class="form-control" id="user" name="username" onkeyup="validateForm()">
					  		</div>
					  		
					  		<div class="form-input-container">
							    <label for="pwd">Password:</label>
							    <input type="password" class="form-control" id="pwd" name="password" onkeyup="validateForm()">
					  		</div>
					  		
					  		<div class="form-input-container">
							    <label for="pwd2">Repeat password:</label>
							    <input type="password" class="form-control" id="pwd2" name="password2" onkeyup="validateForm()">
					  		</div>
					  		
					  		<div class="form-input-container">
							    <label for="first-name">First name:</label>
							    <input type="text" class="form-control" id="first-name" name="firstName" onkeyup="validateForm()">
					  		</div>
					  		
					  		<div class="form-input-container">
							    <label for="last-name">Last name:</label>
							    <input type="text" class="form-control" id="last-name" name="lastName" onkeyup="validateForm()">
					  		</div>
					  		
					  		</div>
					  		<label for="user-type-select">User Type:</label>
					  		<br>
					  		<select id="user-type-select" name="userTypeSelect">
								
								<c:forEach items="${userTypes}" var="userType" >
									<option value="${userType.id}">${userType.typeName}</option>
								</c:forEach>
							</select>
							
							<input type="hidden" name="isCreatedByAdmin" value="true">
							
							<br>
							<div class="form-group" id="submit-button-container">
								<button type="submit" class="btn btn-default auth-action-btn" id="submit-button">Create</button>
							</div>
							
						</form>
						</div>
						
						<div id="error-message-container">
							<c:if test="${requestScope.errorMessage != null}">
								<p>${requestScope.errorMessage}</p>
							</c:if>
						</div>		
						
					</div>
				</div>
			</div>
		</div>
		
		<script>		
			function validateForm() {
				
				var submitButton = document.getElementById("submit-button");
				
				var fieldIds = ["mail", "user", "pwd", "pwd2", "first-name", "last-name"];
				
				for (var i = 0; i < fieldIds.length; i++) {
					if (document.getElementById(fieldIds[i]).value == "" || document.getElementById(fieldIds[i]).value == null) {
						submitButton.disabled = true;
						return;
					}
				}
				
				submitButton.disabled = false;
			}
		</script>

	<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
	
</body>
</html>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>    
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Project Libri | Change Password</title>
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
<link rel="stylesheet" href="${pageContext.request.contextPath}/styles/plStyles.css">

</head>
<body>
	
	<div class="change-pword-container" name=register-container style="heigth:100px">
	<form action="/project-libri/accountAction" method="POST">
		 <div>	
		 
  		<div class="form-group">
		    <label for="pwd">Old password:</label>
		    <input type="password" class="form-control" id="pwd" name="oldPassword">
  		</div>
  		
  		<div class="form-group">
		    <label for="pwd">New password:</label>
		    <input type="password" class="form-control" id="pwd" name="newPassword1">
  		</div>
  		
  		<div class="form-group">
		    <label for="pwd2">Repeat new password:</label>
		    <input type="password" class="form-control" id="pwd2" name="newPassword2">
  		</div>
  		
  		
  		</div>
		
		<div class="form-group" id="login-button-container">
			<button type="submit" class="btn btn-default auth-action-btn" id="login-button">Submit</button>
		</div>
		
	</form>
	</div>
	
	<div id="error-message-container">
		<c:if test="${requestScope.errorMessage != null}">
			<p>${requestScope.errorMessage}</p>
		</c:if>
	</div>
	
	<div id="success-message-container">
		<c:if test="${requestScope.successMessage != null}">
			<p>${requestScope.successMessage}</p>
		</c:if>
	</div>
	
	<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
	
</body>
</html>
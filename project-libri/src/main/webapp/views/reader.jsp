<%@page import="com.projectlibri.jdo.BookEditionDO"%>
<%@page import="com.projectlibri.utils.DBUtils"%>
<%@page import="com.projectlibri.utils.FilesUtils"%>
<%@page import="org.apache.commons.io.FileUtils"%>
<%@page import="java.io.File"%>
<%@page import="com.projectlibri.jdo.UserDO"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<%
UserDO userData = (UserDO) session.getAttribute("userData");

String username = "";
if (userData == null) {
	response.sendRedirect("/project-libri/login");
	return;
} else {
	username = userData.getUsername();	
}

int bookId = Integer.parseInt(request.getParameter("bookId"));
int languageId = Integer.parseInt(request.getParameter("languageId"));
BookEditionDO bookEdition = DBUtils.getBookEdition(bookId, languageId);
pageContext.setAttribute("authorId", bookEdition.getAuthorId());
pageContext.setAttribute("bookId", bookEdition.getTitle());
pageContext.setAttribute("languageId", bookEdition.getLanguageId());

%>
<meta charset="ISO-8859-1">
<link rel="stylesheet" href="${pageContext.request.contextPath}/styles/plStyles.css">
<title>Reader</title>
</head>
<body>
 <div class="book-header">
						<h3><a class="black-anchor" href="authorDetails.jsp?authorId=${authorId}">
						<% out.println(bookEdition.getAuthorFirstName()); %>
						<% out.println(bookEdition.getAuthorLastName()); %> </a></h3>
						<h2><a class="black-anchor" href="bookDetails.jsp?bookId=${authorId}&languageId=${languageId}">
						<% out.println(bookEdition.getLanguageTitle());%></a>
						<% out.println("(" + bookEdition.getYear() + ")"); %></h2>
						<p><% out.println(bookEdition.getPublisher() + ","); %>
						<% out.println(bookEdition.getPublicationYear()); %></p>
						
</div>
<textarea readonly class=transc-text><%out.println(DBUtils.getExtractedTextFromDB(bookId, languageId));%></textarea>

</body>
</html>